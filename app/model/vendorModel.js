'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   VendorMaster = function(vendormaster){
    this.VendorID  = vendormaster.VendorID ;
   this.FirstName  = vendormaster.FirstName ;
   this.LastName  = vendormaster.LastName ;
   this.MobileNumber = vendormaster.MobileNumber ;
   this.Email = vendormaster.Email ;
   this.Address = vendormaster.Address ;
   this.DiscontPercentage = vendormaster.DiscontPercentage;
   this.Active_YN = vendormaster.Active_YN;
   this.SchoolID = vendormaster.SchoolID;
   this.ClassID = vendormaster.ClassID;
   this.UniqueID = vendormaster.UniqueID;
   this.BoardID = vendormaster.BoardID;
   this.Price = vendormaster.Price;
   this.IsActive = vendormaster.IsActive;

   
  

};
VendorMaster.createVendorMaster = function createUser(vendormaster, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_VendorMaster '"+ vendormaster.VendorID +"','"+ vendormaster.FirstName  +"','"+ vendormaster.LastName  +"','"+vendormaster.MobileNumber  +"','"+vendormaster.Email  +"','"+ vendormaster.Address +"','"+ vendormaster.DiscontPercentage +"','"+vendormaster.Active_YN +"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
VendorMaster.getVendorMaster = function createUser(vendormaster, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchVendorMaster '"+vendormaster.VendorID +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};


VendorMaster.getVendorSchoolMapping = function createUser(VendorSchoolMapping, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC fetchVendorSchoolMapping '"+VendorSchoolMapping.VendorID+"','"+VendorSchoolMapping.SchoolID+"','"+VendorSchoolMapping.IsActive+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
VendorMaster.getVendorSetMapping = function createUser(VendorSetMapping, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC fetchVendorSetMapping '"+VendorSetMapping.VendorID+"','"+VendorSetMapping.SchoolID+"','"+VendorSetMapping.ClassID+"', '"+VendorSetMapping.IsActive+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

VendorMaster.createVendorSetMapping = function createUser(VendorSetMapping, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_VendorSetMapping '"+ VendorSetMapping.UniqueID+"','"+ VendorSetMapping.VendorID +"','"+ VendorSetMapping.BoardID  +"','"+VendorSetMapping.SchoolID  +"','"+VendorSetMapping.ClassID  +"','"+ VendorSetMapping.Price +"','"+ VendorSetMapping.IsActive +"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};


VendorMaster.createVendorSchoolMapping = function createUser(VendorSchoolMapping, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_VendorSchoolMapping '"+ VendorSchoolMapping.UniqueID +"','"+ VendorSchoolMapping.VendorID +"','"+ VendorSchoolMapping.BoardID  +"','"+VendorSchoolMapping.SchoolID  +"','"+ VendorSchoolMapping.IsActive +"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};





module.exports= VendorMaster;