'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   ProductDetail = function(productdetail){
    this.ProductID = productdetail.ProductID;
   this.ApplicationID = productdetail.ApplicationID;
   this.BoardID = productdetail.BoardID;
   this.ClassID = productdetail.ClassID;
   this.ProductName = productdetail.ProductName;
   this.Availability = productdetail.Availability;
   this.AuthorName = productdetail.AuthorName;
   this.ISBNNO = productdetail.ISBNNO;
   this.Publisher = productdetail.Publisher;
   this.NoOfPages = productdetail.NoOfPages;
   this.PublicationYear = productdetail.PublicationYear;
   this.Binding = productdetail.Binding;
   this.Imprint = productdetail.Imprint;
   this.Edition = productdetail.Edition;
   this.Condition = productdetail.Condition;
   this.Subject = productdetail.Subject;
   this.Language = productdetail.Language;
   this.Image1 = productdetail.Image1;
   this.Image2 = productdetail.Image2;
   this.Image3 = productdetail.Image3;
   this.ProductDescription = productdetail.ProductDescription;
   this.Active_YN = productdetail.Active_YN;
   this.EnterBy = productdetail.EnterBy;
   this.EnterFrom  = productdetail.EnterFrom ;
   this.Price  = productdetail.Price ;
   this.Limit=productdetail.Limit;
   this.OrderID  = productdetail.OrderID ;
   this.UserID  = productdetail.UserID ;
   this.Subtotal  = productdetail.Subtotal ;
   this.ShippingCharge  = productdetail.ShippingCharge ;
   this.TotalAmount   = productdetail.TotalAmount  ;
   this.PaymentMode   = productdetail.PaymentMode  ;
   this.Address =productdetail.Address ;
   this.OrderDetails=productdetail.OrderDetails;
   this.Status=productdetail.Status;
   this.ReferenceID=productdetail.ReferenceID;
};
ProductDetail.createProductDetail = function createUser(productdetail, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_ProductDetail "+productdetail.ProductID +","+productdetail.ApplicationID +","+productdetail.BoardID +","+productdetail.ClassID +",'"+productdetail.ProductName +"','"+productdetail.Availability +"','"+productdetail.AuthorName +"','"+productdetail.ISBNNO +"','"+productdetail.Publisher +"','"+productdetail.NoOfPages +"','"+productdetail.PublicationYear +"','"+productdetail.Binding +"','"+productdetail.Imprint +"','"+productdetail.Edition +"','"+productdetail.Condition +"','"+productdetail.Subject +"','"+productdetail.Language +"','"+productdetail.Image1 +"','"+productdetail.Image2 +"','"+productdetail.Image3 +"','"+productdetail.ProductDescription +"','"+productdetail.Active_YN +"','"+productdetail.EnterBy +"','"+productdetail.EnterFrom +"','"+productdetail.Price+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetail.getboardclasswiseProductDetail = function createUser(boardclasswiseproduct, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Fetchboardclasswiseproduct "+boardclasswiseproduct.BoardID+", "+boardclasswiseproduct.ClassID+","+boardclasswiseproduct.ApplicationID +"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetail.getboardwiseproductdetails = function createUser(boardwiseproduct, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Fetchboardwiseproduct "+boardwiseproduct.BoardID+", "+boardwiseproduct.ApplicationID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

ProductDetail.getproductbyiddetails = function createUser(productbyid, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Fetchproductbyid "+productbyid.ProductID+", "+productbyid.ApplicationID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

ProductDetail.getBoardClassdetails = function createUser(BoardClass, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchBoardClass "+BoardClass.BoardID+","+BoardClass.ClassID+","+BoardClass.ApplicationID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetail.getmaxproductid = function createUser(maxproductid, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchMaxProductID", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetail.getlatestproduct = function createUser(latestproduct, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchLatestProduct '"+latestproduct.ProductID+"','"+latestproduct.ApplicationID+"','"+latestproduct.Limit+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};




ProductDetail.getSubjectwiseproduct = function createUser(Subjectwiseproduct, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchSubjectwiseproduct '"+Subjectwiseproduct.Subject +"','"+Subjectwiseproduct.ApplicationID  +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

ProductDetail.createOrders = function createUser(Orders, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_Orders "+Orders.OrderID  +","+Orders.UserID  +","+Orders.Subtotal  +",'"+Orders.ShippingCharge   +"','"+Orders.TotalAmount  +"','"+Orders.PaymentMode  +"','"+Orders.Address  +"','"+Orders.Status+"','"+Orders.ReferenceID+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
            var orderID = res.recordset[0].OrderID;
            const tvp = new sql.Table();
            tvp.columns.add('ORDER_ID', sql.Int);
            tvp.columns.add('PRODUCT_ID', sql.Int);
            tvp.columns.add('QUANTITY', sql.Int);

            var orderDetails = Orders.OrderDetails;
           
            for(var i = 0; i < orderDetails.length; i++) {
                tvp.rows.add(orderID, orderDetails[i].PRODUCT_ID, orderDetails[i].QUANTITY);
            }
            connection = new sql.Request();
            connection.input('details', tvp);
            connection.execute("InsOrderDetails", function (err1, res1) {
                console.log("Error: " + err1);
               // console.log("Response: " + res1);
            });
        }
    });   
};

ProductDetail.getBestSellingBooks = function createUser(BestSellingBooks, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchBestSellingBooks '"+BestSellingBooks.ProductID  +"','"+BestSellingBooks.ApplicationID +"','"+BestSellingBooks.Limit+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};


module.exports= ProductDetail;