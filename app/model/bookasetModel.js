'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   BookASet = function(bookaset){
    this.UniqueID = bookaset.UniqueID;
   this.BaordID = bookaset.BaordID;
   this.SchoolID = bookaset.SchoolID;
   this.ClassID = bookaset.ClassID;
   this.Name = bookaset.Name;
   this.MobileNumber = bookaset.MobileNumber;
   this.Status = bookaset.Status;
   this.Email = bookaset.Email;
   this.ReferenceID=bookaset.ReferenceID;
   this.Subject=bookaset.Subject;
   this.Address=bookaset.Address;
   this.Price=bookaset.Price;
   this.CallingDate=bookaset.CallingDate;
   this.RecievedAmount=bookaset.RecievedAmount;
   this.VendorPrice=bookaset.VendorPrice;
   this.OptionalSubject=bookaset.OptionalSubject;
};
BookASet.createBookASet = function createUser(bookaset, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_BookASet '"+ bookaset.UniqueID+"','"+ bookaset.BaordID +"','"+ bookaset.SchoolID +"','"+bookaset.ClassID +"','"+bookaset.Name +"','"+ bookaset.MobileNumber +"','"+bookaset.ReferenceID+"','"+ bookaset.Subject +"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
BookASet.getbookaset = function createUser(bookaset, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchBookASet'"+bookaset.UniqueID +"','"+bookaset.Status +"' ", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

BookASet.updateBookASet = function createUser(updbookaset, result) {
  
    var connection = new sql.Request();
    connection.query("EXEC updateBookaSet  "+ updbookaset.UniqueID+","+ updbookaset.BaordID +","+ updbookaset.SchoolID+","+ updbookaset.ClassID+",'"+ updbookaset.Name+"','"+ updbookaset.MobileNumber+"','"+ updbookaset.Address+"','"+ updbookaset.Status+"','"+ updbookaset.Email+"','"+ updbookaset.Subject+"','"+updbookaset.Price +"','"+updbookaset.CallingDate+"','"+updbookaset.RecievedAmount+ "','"+updbookaset.VendorPrice+"','"+updbookaset.OptionalSubject+"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    }); 
};
BookASet.getcheckAvailability = function createUser(bookaset, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC checkAvailabilityBookset'"+bookaset.BoardID +"','"+bookaset.SchoolID +"','"+bookaset.ClassID+"' ", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

module.exports= BookASet;