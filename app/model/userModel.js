'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   User = function(user){
   this.UserDetailID  = user.UserDetailID ;
   this.FirstName  = user.FirstName ;
   this.LastName  = user.LastName ;
   this.EmailAddress  = user.EmailAddress ;
   this.Address  = user.Address ;
   this.StreetAddress2  = user.StreetAddress2 ;
   this.City  = user.City ;
   this.State  = user.State ;
   this.PostalCode  = user.PostalCode ;
   this.Country  = user.Country ;
   this.Telephone  = user.Telephone ;
   this.UniqueID  = user.UniqueID ;
   this.MobileNumber  = user.MobileNumber ;
   this.Otp  = user.Otp ;
   this.Flag  = user.Flag ;
   this.PermanentUserID   = user.PermanentUserID  ;
   this.TempUserID   = user.TempUserID  ;
   this.UserID   = user.UserID  ;

};
User.createUser = function createUser(user, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_UserDetail "+user.UserDetailID  +",'"+user.FirstName  +"','"+user.LastName +"','"+user.EmailAddress +"','"+user.Address  +"','"+user.StreetAddress2  +"','"+user.City  +"','"+user.State  +"','"+user.PostalCode  +"','"+user.Country  +"','"+user.Telephone  +"','"+user.TempUserID+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

User.createMobileVerification = function createUser(MobileVerification, result) {
   
    var connection = new sql.Request();
    console.log(MobileVerification);
    connection.query("EXEC Insupd_MobileVerification '"+MobileVerification.MobileNumber+"','"+ MobileVerification.Otp  +"','"+MobileVerification.Flag  +"';", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           res.customStatus = "true";
            result(null, res);
        }
    });   
};

User.createTempUser = function createUser(TempUser, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC UpdateTempUser '"+TempUser.PermanentUserID    +"','"+TempUser.TempUserID   +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
User.getUserDetail = function createUser(UserDetail, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchUserDetail '"+UserDetail.UserDetailID+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
User.getUserverificationDetail = function createUser(UserDetail, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC fetchPendingVerification", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

User.getUserdata = function createUser(Userdata, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchUserData", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

User.VisitorCount = function createUser(visitorcount , result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_VisitorCount "+visitorcount.UniqueID +","+visitorcount.UserID +"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

module.exports= User;