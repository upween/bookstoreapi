'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   BoardClass = function(boardclass){
    this.BoardID = boardclass.BoardID;
   this.ApplicationID = boardclass.ApplicationID;
   this.ClassID = boardclass.ClassID;
   this.SubjectID = boardclass.SubjectID;
   this.SchoolID  = boardclass.SchoolID ;

   

    
  
};

BoardClass.getboardclass = function createUser(boardclass, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchboardwiseClass '"+boardclass.BoardID +"','"+boardclass.ApplicationID +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};



BoardClass.getclassmaster = function createUser(classmaster, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchClassMaster '"+classmaster.ClassID +"','"+classmaster.ApplicationID   +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};


BoardClass.getboardmaster = function createUser(boardmaster, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchBoardMaster '"+boardmaster.BoardID  +"','"+boardmaster.ApplicationID    +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
  

BoardClass.getsubjectmaster = function createUser(subjectmaster, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchSubjectMaster '"+subjectmaster.SubjectID   +"','"+subjectmaster.ApplicationID    +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

BoardClass.getSchoolMaster = function createUser(SchoolMaster, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchSchoolMaster '"+SchoolMaster.SchoolID     +"','"+SchoolMaster.BoardID      +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

BoardClass.getHighSecondarySubject = function createUser(HighSecondarySubject, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchHighSecondarySubject '"+ HighSecondarySubject.ID +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

  
module.exports= BoardClass;