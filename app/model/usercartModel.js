'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   UserCart = function(usercart){
    this.UniqueID = usercart.UniqueID;
   this.UserID = usercart.UserID;
   this.Product_ID = usercart.Product_ID;
   this.Quantity = usercart.Quantity;
   this.EntryFrom = usercart.EntryFrom;
   this.Price = usercart.Price;
   this.DiscountedPrice = usercart.DiscountedPrice;
 
 
};
UserCart.createUserCart = function createUser(usercart, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_UserCartItems '"+ usercart.UniqueID +"','"+usercart.UserID +"','"+usercart.Product_ID +"','"+usercart.Quantity+"','"+ usercart.EntryFrom +"','"+usercart.Price +"','"+ usercart.DiscountedPrice +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
UserCart.getUserCartbyId = function createUser(UserCartbyId, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchCartItemByUserId "+UserCartbyId.UserID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
UserCart.deleteCartItems = function createUser(deletecartitem, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC DeleteCartItems "+deletecartitem.UserID+","+deletecartitem.UniqueID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
module.exports= UserCart;