'use strict';
var AndroidReg = require('../model/AndroidModel.js');


  exports.create_a_AndroidReg = function(req, res) {
    var InsertAndroidRegID= new AndroidReg(req.body);
   
    AndroidReg.createAndroidReg(InsertAndroidRegID, function(err, AndroidReg) {
        if (err){
          res.send(err);
        }
          else{
        res.json(AndroidReg);
          }
      });
    
  };

  exports.read_a_AndroidReg = function(req, res) {
  
  
    AndroidReg.getAndroidReg(req.params, function(err, AndroidReg) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(AndroidReg);
        }
    });
  };