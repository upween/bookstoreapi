'use strict';
var VendorMaster = require('../model/vendorModel.js');


  exports.create_a_VendorMaster = function(req, res) {
    var newVendorMaster= new VendorMaster(req.body);
   
    VendorMaster.createVendorMaster(newVendorMaster, function(err, vendormaster) {
        if (err){
          res.send(err);
        }
          else{
        res.json(vendormaster);
          }
      });
    
  };
  exports.read_a_VendorMaster = function(req, res) {
  
  
    VendorMaster.getVendorMaster(req.params, function(err,vendormaster) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(vendormaster);
        }
    });
  };


  exports.read_a_VendorSchoolMapping = function(req, res) {
  
  
    VendorMaster.getVendorSchoolMapping(req.params, function(err,VendorSchoolMapping) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(VendorSchoolMapping);
        }
    });
  };

  exports.read_a_VendorSetMapping = function(req, res) {
  
  
    VendorMaster.getVendorSetMapping(req.params, function(err,VendorSetMapping) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(VendorSetMapping);
        }
    });
  };

  exports.create_a_VendorSetMapping = function(req, res) {
    var Insupd_VendorSetMapping= new VendorMaster(req.body);
   
    VendorMaster.createVendorSetMapping(Insupd_VendorSetMapping, function(err, VendorSetMapping) {
        if (err){
          res.send(err);
        }
          else{
        res.json(VendorSetMapping);
          }
      });
    
  };


  exports.create_a_VendorSchoolMapping = function(req, res) {
    var Insupd_VendorSchoolMapping= new VendorMaster(req.body);
   
    VendorMaster.createVendorSchoolMapping(Insupd_VendorSchoolMapping, function(err, VendorSchoolMapping) {
        if (err){
          res.send(err);
        }
          else{
        res.json(VendorSchoolMapping);
          }
      });
    
  };

  

