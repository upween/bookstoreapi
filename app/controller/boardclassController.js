'use strict';
var BoardClass = require('../model/boardclassModel.js');



  exports.read_a_boardclass = function(req, res) {
  
  
    BoardClass.getboardclass(req.params, function(err,boardclass) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(boardclass);
        }
    });
  };


  exports.read_a_classmaster = function(req, res) {
  
  
    BoardClass.getclassmaster(req.params, function(err,classmaster) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(classmaster);
        }
    });
  };


  exports.read_a_boardmaster = function(req, res) {
  
  
    BoardClass.getboardmaster(req.params, function(err,boardmaster) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(boardmaster);
        }
    });
  };

  exports.read_a_subjectmaster = function(req, res) {
  
  
    BoardClass.getsubjectmaster(req.params, function(err,subjectmaster) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(subjectmaster);
        }
    });
  };


  exports.read_a_SchoolMaster = function(req, res) {
  
  
    BoardClass.getSchoolMaster(req.params, function(err,SchoolMaster) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(SchoolMaster);
        }
    });
  };

  
  exports.read_a_HighSecondarySubject = function(req, res) {
  
  
    BoardClass.getHighSecondarySubject(req.params, function(err,HighSecondarySubject) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(HighSecondarySubject);
        }
    });
  };


  
 