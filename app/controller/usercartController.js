'use strict';
var UserCart = require('../model/usercartModel.js');


  exports.create_a_UserCart = function(req, res) {
    var Insupd_UserCartItems= new UserCart(req.body);
   
    UserCart.createUserCart(Insupd_UserCartItems, function(err, usercart) {
        if (err){
          res.send(err);
        }
          else{
        res.json(usercart);
          }
      });
    
  };
  exports.read_a_usercartbyid= function(req, res) {
  
  
    UserCart.getUserCartbyId(req.params, function(err, cartitems) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(cartitems);
        }
    });
  };
  exports.delete_a_usercartbyid= function(req, res) {
  
  
    UserCart.deleteCartItems(req.params, function(err, cartitems) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(cartitems);
        }
    });
  };