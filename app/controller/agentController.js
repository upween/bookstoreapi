'use strict';
var AgentMaster = require('../model/agentModel.js');


  exports.create_a_AgentMaster = function(req, res) {
    var newAgentMaster= new AgentMaster(req.body);
   
    AgentMaster.createAgentMaster(newAgentMaster, function(err, agentmaster) {
        if (err){
          res.send(err);
        }
          else{
        res.json(agentmaster);
          }
      });
    
  };


  exports.read_a_AgentMaster = function(req, res) {
  
  
    AgentMaster.getAgentMaster(req.params, function(err,agentmaster) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(agentmaster);
        }
    });
  };


  