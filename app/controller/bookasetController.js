'use strict';
var BookASet = require('../model/bookasetModel.js');


  exports.create_a_BookASet = function(req, res) {
    var Insupd_BookASet= new BookASet(req.body);
   
    BookASet.createBookASet(Insupd_BookASet, function(err, bookaset) {
        if (err){
          res.send(err);
        }
          else{
        res.json(bookaset);
          }
      });
    
  };
  exports.read_a_BookASet = function(req, res) {
  
  
    BookASet.getbookaset(req.params, function(err,bookaset) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(bookaset);
        }
    });
  };
  exports.update_a_BookASet = function(req, res) {
    var updateBookaSet= new BookASet(req.body);
   
    BookASet.updateBookASet(updateBookaSet, function(err, bookaset) {
        if (err){
          res.send(err);
        }
          else{
        res.json(bookaset);
          }
      });
    
  };
  exports.read_a_checkAvailibiltiy = function(req, res) {
  
  
    BookASet.getcheckAvailability(req.params, function(err,bookaset) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(bookaset);
        }
    });
  };
