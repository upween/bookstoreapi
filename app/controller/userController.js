'use strict';
var User = require('../model/userModel.js');


  exports.create_a_User = function(req, res) {
    var Insupd_UserDetail= new User(req.body);
   
    User.createUser(Insupd_UserDetail, function(err, user) {
        if (err){
          res.send(err);
        }
          else{
        res.json(user);
          }
      });
    
  };


  exports.create_a_MobileVerification = function(req, res) {
    var Insupd_MobileVerification= new User(req.body);
   
    User.createMobileVerification(Insupd_MobileVerification, function(err, MobileVerification) {
        if (err){
          res.send(err);
        }
          else{
        res.json(MobileVerification);
          }
      });
    
  };


  exports.create_a_TempUser = function(req, res) {
    var UpdateTempUser= new User(req.body);
   
    User.createTempUser(UpdateTempUser, function(err, TempUser) {
        if (err){
          res.send(err);
        }
          else{
        res.json(TempUser);
          }
      });
    
  };
  exports.read_a_UserDetail = function(req, res) {
  
  
    User.getUserDetail(req.params, function(err,UserDetail) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(UserDetail);
        }
    });
  };

  exports.read_a_pendingUserverification = function(req, res) {
  
  
    User.getUserverificationDetail(req.params, function(err,UserDetail) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(UserDetail);
        }
    });
  };


  
  exports.read_a_UserData = function(req, res) {
  
  
    User.getUserdata(req.params, function(err,UserDetail) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(UserDetail);
        }
    });
  };

  exports.create_a_VisitorCount = function(req, res) {
    var Insupd_VisitorCount= new User(req.body);
   
    User.VisitorCount(Insupd_VisitorCount, function(err, VisitorCount) {
        if (err){
          res.send(err);
        }
          else{
        res.json(VisitorCount);
          }
      });
    
  };


  



 