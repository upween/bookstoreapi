'use strict';
var ProductDetail = require('../model/productModel.js');


  exports.create_a_ProductDetail = function(req, res) {
    var Insupd_ProductDetail= new ProductDetail(req.body);
   
    ProductDetail.createProductDetail(Insupd_ProductDetail, function(err, productdetail) {
        if (err){
          res.send(err);
        }
          else{
        res.json(productdetail);
          }
      });
    
  };
  exports.read_a_boardclasswiseproduct = function(req, res) {
  
  
    ProductDetail.getboardclasswiseProductDetail(req.params, function(err, boardclasswiseproduct) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(boardclasswiseproduct);
        }
    });
  };

  
  exports.read_a_boardwiseproduct = function(req, res) {
  
  
    ProductDetail.getboardwiseproductdetails(req.params, function(err, boardwiseproduct) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(boardwiseproduct);
        }
    });
  };

  exports.read_a_productbyid = function(req, res) {
  
  
    ProductDetail.getproductbyiddetails(req.params, function(err, productbyid) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(productbyid);
        }
    });
  };

  exports.read_a_BoardClass= function(req, res) {
  
  
    ProductDetail.getBoardClassdetails(req.params, function(err, BoardClass) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(BoardClass);
        }
    });
  };
  exports.read_a_maxproductid= function(req, res) {
  
  
    ProductDetail.getmaxproductid(req.params, function(err, productid) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(productid);
        }
    });
  };
  exports.read_a_latestproduct= function(req, res) {
  
  
    ProductDetail.getlatestproduct(req.params, function(err, product) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(product);
        }
    });
  };

  // exports.read_a_classmaster = function(req, res) {
  
  
  //   ProductDetail.getclassmaster(req.params, function(err,classmaster) {
  //     if (err){
  //       res.send(err);
  //     }
  //       else{
     
  //       res.json(classmaster);
  //       }
  //   });
  // };


  exports.read_a_Subjectwiseproduct = function(req, res) {
  
  
    ProductDetail.getSubjectwiseproduct(req.params, function(err,Subjectwiseproduct) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(Subjectwiseproduct);
        }
    });
  };

  exports.create_a_Orders = function(req, res) {
    var Insupd_Orders= new ProductDetail(req.body);
   
    ProductDetail.createOrders(Insupd_Orders, function(err, Orders) {
        if (err){
          res.send(err);
        }
          else{
        res.json(Orders);
          }
      });
    
  };

  exports.read_a_BestSellingBooks = function(req, res) {
  
  
    ProductDetail.getBestSellingBooks(req.params, function(err, BestSellingBooks) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(BestSellingBooks);
        }
    });
  };
