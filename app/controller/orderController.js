'use strict';
var Order = require('../model/orderModel.js');


  exports.read_a_orderInvoice = function(req, res) {
  
  
    Order.getorderInvoice(req.params, function(err, orderInvoice) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(orderInvoice);
        }
    });
  };
  exports.read_a_orderbyuser = function(req, res) {
  
  
    Order.getorderbyuserid(req.params, function(err, orders) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(orders);
        }
    });
  };