'use strict';
import TokenValidator from '../../middleware/TokenValidator'
import Axios from 'axios';
module.exports = function (app) {
  var rolemenu = require('../controller/appController');
  var productdetail = require('../controller/productController');
  var boardclass =require('../controller/boardclassController');
  var user =require('../controller/userController');
  var bookaset =require('../controller/bookasetController');
  var AndroidReg =require('../controller/AndroidController');
  var orderInvoice =require('../controller/orderController');
  var contactUs =require('../controller/contactusController');
  var contactUs =require('../controller/contactusController');
  var vendorMaster =require('../controller/vendorController');
  var agentMaster =require('../controller/agentController');
  var vendorReport =require('../controller/reportController');


  




  var UserCart =require('../controller/usercartController');
  app.use(TokenValidator)
//Role Menu Routes Begin
  app.route('/rolemenu/:ApplicationID').get(rolemenu.read_a_rolemenu);

  //Product details Routes Begin
  app.route('/productdetail').post(productdetail.create_a_ProductDetail);
 
  app.route('/boardclasswiseproduct/:BoardID/:ClassID/:ApplicationID').get(productdetail.read_a_boardclasswiseproduct );
  // UserCart Routes Begin
  app.route('/usercart').post(UserCart.create_a_UserCart);
  app.route('/usercart/:UserID').get(UserCart.read_a_usercartbyid);
  app.route('/deletecartitems/:UserID/:UniqueID').get(UserCart.delete_a_usercartbyid);
  app.route('/boardwiseproduct/:BoardID/:ApplicationID').get(productdetail.read_a_boardwiseproduct);

  app.route('/productdetailsbyid/:ProductID/:ApplicationID').get(productdetail.read_a_productbyid);

  app.route('/BoardClassdetails/:BoardID/:ClassID/:ApplicationID').get(productdetail.read_a_BoardClass);
  app.route('/getmaxproductid').get(productdetail.read_a_maxproductid);
  app.route('/latestproduct/:ProductID/:ApplicationID/:Limit').get(productdetail.read_a_latestproduct);
  app.route('/BoardClass/:BoardID/:ApplicationID').get(boardclass.read_a_boardclass);


  app.route('/HighSecondarySubject/:ID').get(boardclass.read_a_HighSecondarySubject);


  app.route('/ClassMaster/:ClassID/:ApplicationID').get(boardclass.read_a_classmaster);

  app.route('/boardmaster/:BoardID/:ApplicationID').get(boardclass.read_a_boardmaster);

  app.route('/subjectmaster/:SubjectID/:ApplicationID').get(boardclass.read_a_subjectmaster);



  app.route('/Subjectwiseproduct/:Subject/:ApplicationID').get(productdetail.read_a_Subjectwiseproduct);

  app.route('/user').post(user.create_a_User);

  app.route('/pendingUserVerification').get(user.read_a_pendingUserverification);

  app.route('/userdata').get( user.read_a_UserData);

  app.route('/MobileVerification').post(user.create_a_MobileVerification);

  app.route('/visitorcount').post(user.create_a_VisitorCount);

  app.route('/TempUser').post(user.create_a_TempUser);

  app.route('/SchoolMaster/:SchoolID/:BoardID').get(boardclass.read_a_SchoolMaster);

// BookASet Routes Begin

app.route('/bookaset').post(bookaset.create_a_BookASet);

app.route('/Orders').post(productdetail.create_a_Orders);

app.route('/AndroidReg').post(AndroidReg.create_a_AndroidReg);

app.route('/AndroidReg/:UserID').get(AndroidReg.read_a_AndroidReg);

app.route('/BookASet/:UniqueID/:Status').get(bookaset.read_a_BookASet);

app.route('/updatebookaset').post(bookaset.update_a_BookASet);

app.route('/ContactUs/:ContactID').get(contactUs.read_a_ContactUs);
   

app.route('/BestSellingBooks/:ProductID/:ApplicationID/:Limit').get(productdetail.read_a_BestSellingBooks);
app.get('/sendsms',(req,res)=>{
  let url=req.url;
   let parameter = url.split("?")[1];
let data =JSON.parse(decodeURI(parameter));
console.log(data);
Axios({
    method: 'post',
    url: 'https://api.msg91.com/api/v2/sendsms',
    data: data,
    headers:{
        "authkey":"264840AT4bfsGwDs5c74e909",
        "Content-Type":"application/json"
    }
  }).then((response)=>{
    console.log(response);
    res.status(200).send({
      status:"success"
      })
  }).catch((err)=>{
    console.log(err)
    res.status(500).send({
      "data":"",
      "err":"some error"
    })
  })
});
app.route('/user/:UserDetailID').get(user.read_a_UserDetail);
app.route('/orderInvoice/:OrderID').get(orderInvoice.read_a_orderInvoice);
app.route('/orderbyuser/:UserID').get(orderInvoice.read_a_orderbyuser);
app.route('/contactus').post(contactUs.create_a_ContactUs);
app.route('/contactus').post(contactUs.create_a_ContactUs);

app.route('/vendormaster').post(vendorMaster.create_a_VendorMaster);

app.route('/vendormaster/:VendorID').get(vendorMaster.read_a_VendorMaster);

app.route('/VendorSchoolMapping/:VendorID/:SchoolID/:IsActive').get(vendorMaster.read_a_VendorSchoolMapping);
app.route('/VendorSetMapping').post(vendorMaster.create_a_VendorSetMapping);

app.route('/VendorSchoolMapping').post(vendorMaster.create_a_VendorSchoolMapping);


app.route('/VendorSetMapping/:VendorID/:SchoolID/:ClassID/:IsActive').get(vendorMaster.read_a_VendorSetMapping);


app.route('/agentmaster').post(agentMaster.create_a_AgentMaster);

app.route('/agentmaster/:AgentID').get(agentMaster.read_a_AgentMaster);

app.route('/vendorreport/:UniqueID').get(vendorReport.read_a_VendorReport);
app.route('/checkAvailibility/:BoardID/:SchoolID/:ClassID').get(bookaset.read_a_checkAvailibiltiy);


};
